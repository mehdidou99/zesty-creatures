#ifndef AI_HPP_INCLUDED
#define AI_HPP_INCLUDED

#include "menus.hpp"
#include "game_data.hpp"

namespace AI
{
	/**
	* @brief Computes the choice of the AI for a given battle round.
	* @details The current AI simply chooses a random attack.
	* 
	* @param[in] game_data An instance of the type GameData that holds the random number generator.
	* @param[in] trainer The trainer controlled by the AI.
	* @param[in] opponent The opponent of the trainer that the AI controls.
	* @returns The stack of all the choices made in the menus and sub-menus.
	*/
	ChoicesStack choose_round_action(GameData const& game_data, Trainer const& trainer, Trainer const& opponent);

	/**
	* @brief Computes the choice of the AI to handle a KO.
	* @details This function is'nt implemented yet.
	*
	* @param[in] game_data An instance of the type GameData that holds the random number generator.
	* @param[in] trainer The trainer controlled by the AI.
	*
	* @returns The stack of all the choices made in the menus and sub-menus.
	*/
	ChoicesStack choose_ko_handling_action(GameData const& game_data, Trainer const& trainer);
}

#endif // !AI_HPP_INCLUDED

