#ifndef STAGE_HPP_INCLUDED
#define STAGE_HPP_INCLUDED

#include "entities.hpp"

#include <vector>

/// @typedef StageID
using StageID = int;

/**
* @struct Stage
* @brief Represents a stage.
*/
struct Stage
{
	StageID id;
	/// @brief The trainers of the stage.
	std::vector<Trainer> trainers;
	/// @brief The wild creatures of the stage.
	std::vector<Creature> wild_creatures;
	/// @brief The probability of a wild encounter.
	double wild_encounter_prob;
	/// @brief The number of random draws for wild encounters that the player must complete in order to leave the stage.
	int mandatory_wild_encounters;
};

#endif // !STAGE_HPP_INCLUDED

