#ifndef MENUS_HPP_INCLUDED
#define MENUS_HPP_INCLUDED

#include "entities.hpp"

#include <vector>

/// @typedeg MenuIndex
using MenuIndex = int;
/// @typedef ChoicesStack
using ChoicesStack = std::vector<MenuIndex>;

/// @enum BattleChoice
enum class BattleChoice
{
	Attack, Creatures, Bag, Run
};

/**
* @brief Launches a battle round menu and returns the choice.
*
* @param[in] trainer The trainer whose controller must choose.
*
* @returns The stack of choices from the menu and its submenus.
*/
ChoicesStack battle_menu(Trainer const& trainer);
/**
* @brief Launches an attack choice menu and returns the choice.
*
* @param[in] attacks The attacks to choose from.
*
* @returns The stack of choices from the menu and its submenus.
*/
ChoicesStack attack_menu(Creature::AttackList const& attacks);
/**
* @brief Launches a Creature choice menu and returns the choice.
*
* @param[in] trainer The trainer whose controller must choose.
*
* @returns The stack of choices from the menu and its submenus.
*/
ChoicesStack creatures_menu(Trainer const& trainer);
/**
* @brief Launches a Bag menu and returns the choice.
*
* @param[in] bag The bag to choose from.
*
* @returns The stack of choices from the menu and its submenus.
*/
ChoicesStack bag_menu(Bag const& bag);
/**
* @brief Launches a ko handling menu and returns the choice.
*
* @param[in] trainer The trainer whose controller must choose.
*
* @returns The stack of choices from the menu and its submenus.
*/
ChoicesStack ko_menu(Trainer const& trainer);

#endif // !MENUS_HPP_INCLUDED
