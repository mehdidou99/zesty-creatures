#ifndef WORLD_HPP_INCLUDED
#define WORLD_HPP_INCLUDED

#include "stage.hpp"

#include <vector>

/**
* @struct World
* @brief Holds a map of the world.
*/
struct World
{
	/// @brief The flattened map.
	std::vector<StageID> world;
	/// @brief The height of the world.
	std::size_t height;
	/// @brief The width of the world.
	std::size_t width;
};

/**
* @brief Helper function to convert 2D coordinates into flattened index.
*
* @param x The absciss.
* @param y The ordinate.
* @param The height of the World.
*
* @returns The flattened index.
*/
std::size_t coord_to_index(std::size_t x, std::size_t y, std::size_t height);
/**
* @brief Helper function to convert a flattened index into 2D coordinates.
*
* @param i The index.
* @param The height of the World.
*
* @returns The coordinates.
*/
std::pair<std::size_t, std::size_t> index_to_coord(std::size_t i, std::size_t height);

/**
* @brief Helper function that gives access to the id of a stage on the map.
*
* @param world The map.
* @param i The index.
*
* @returns A reference to the stage id at index `i`.
*/
StageID& get_stageID(World& world, std::size_t i);
/**
* @brief Helper function that gives access to the id of a stage on the map.
*
* @param world The map.
* @param x The absciss.
* @param y The ordinate.
*
* @returns A reference to the stage id at coordinates `(x, y)`.
*/
StageID& get_stageID(World& world, std::size_t x, std::size_t y);



#endif // !WORLD_HPP_INCLUDED
