#include <iostream>
#include <string>
#include <vector>

/**
* @brief try to perform a secure input from a stream.
*
* @tparam T The type of the variable to assign the input to.
*
* @param is The source stream.
* @param variable The variable to assign to.
*
* @returns `true` if the secure input succeeded, `false` if it failed.
*/
template<typename T>
bool secure_input(std::istream& is, T& variable);

/**
* @brief Tries to perform a secure input from a stream respecting a predicate.
*
* @tparam T The type of the variable to assign the input to.
* @tparam PredicateT The type of the predicate.
*
* @param is The source stream.
* @param variable The variable to assign to.
* @param predicate The predicate to use.
*
* @returns `true` if the secure input succeeded, `false` if it failed.
*/
template<typename T, typename PredicateT>
bool secure_input(std::istream& is, T& variable, PredicateT predicate);	

/**
* @brief Splits a string given a custom delimiter.
*
* @param[in] s The string to split.
* @param[in] delimiter The delimiter to use.
*
* @returns The list of the parts of the split.
*/
std::vector<std::string> split(std::string const& s, char delimiter);

#include "utils.tpp"
