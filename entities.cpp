#include "entities.hpp"

/// @copydoc
bool is_ko(Creature const & creature)
{
	return creature.hp <= 0;
}
