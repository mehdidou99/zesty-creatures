#ifndef RANDOM_ENGINE_HPP_INCLUDED
#define RANDOM_ENGINE_HPP_INCLUDED

#include <random>

using RandomEngine = std::mt19937;

inline RandomEngine initialize_random_engine()
{
	return RandomEngine{ std::random_device{}() };
}

inline auto random_engine = initialize_random_engine();

#endif // !RANDOM_ENGINE_HPP_INCLUDED

