#include "battle.hpp"

#include "menus.hpp"
#include "UI.hpp"
#include "AI.hpp"
#include "game_data.hpp"
#include "bag.hpp"
#include "random_engine.hpp"

#include <cassert>
#include <algorithm>
#include <random>

/// @copydoc
BattleResult battle(GameData const& game_data, Trainer& t1, Trainer& t2)
{
	while (true)
	{
		set_first_fighter(t1);
		set_first_fighter(t2);
		if (!battle_round(game_data, t1, t2))
			return BattleResult::FirstEscaped;

		auto ko_handling_result = handle_ko(game_data, t2, t1.wild);
		if (ko_handling_result == KOHandlingResult::AllKO)
			return BattleResult::FirstWon;
		if (ko_handling_result == KOHandlingResult::Run)
			return BattleResult::SecondEscaped;
		if (t2.captured)
			return BattleResult::SecondCaptured;

		if (!battle_round(game_data, t2, t1))
			return BattleResult::SecondEscaped;

		ko_handling_result = handle_ko(game_data, t1, t2.wild);
		if (ko_handling_result == KOHandlingResult::AllKO)
			return BattleResult::SecondWon;
		if (ko_handling_result == KOHandlingResult::Run)
			return BattleResult::FirstEscaped;
		if (t1.captured)
			return BattleResult::FirstCaptured;
	}
}

void set_first_fighter(Trainer & trainer)
{
	auto first_able = std::find_if(std::begin(trainer.creatures), std::end(trainer.creatures), [](Creature const& c) { return !is_ko(c); });
	assert(first_able != std::end(trainer.creatures) && "You shouldn't have launched a battle having one of the trainers without any creature able to fight.");
	trainer.current_creature = first_able;
}

/// @copydoc
bool battle_round(GameData const& game_data, Trainer& t1, Trainer& t2)
{
	auto controller{ t1.controller };

	ChoicesStack choices{};
	if (controller == Controller::Player)
		choices = battle_menu(t1);
	else if (controller == Controller::AI)
		choices = AI::choose_round_action(game_data, t1, t2);

	if (choices[0] == 0)
		apply_attack(game_data, t1.current_creature->attacks[choices[1]], *t1.current_creature, *t2.current_creature);
	else if (choices[0] == 1)
	{
		auto new_fighter = std::begin(t1.creatures) + choices[1];
		if (new_fighter == t1.current_creature)
		{
			UI::display("Cette cr�ature est d�j� au combat !");
			return battle_round(game_data, t1, t2);
		}
		t1.current_creature = new_fighter;
	}
	else if (choices[0] == 2)
	{
		if (!apply_effect(game_data.objects, t1.bag[choices[1]], t1, t2))
			return battle_round(game_data, t1, t2);
	}
	else if (choices[0] == 3)
	{
		if (t2.wild)
			return false;

		if (t1.controller == Controller::Player)
			UI::display("Vous ne pouvez pas vous �chapper d'un duel de dresseurs !");

		return battle_round(game_data, t1, t2);
	}
	else
		assert(false && "You shouldn't have reached this point.");

	return true;
}

/// @copydoc
void apply_attack(GameData const& game_data, Attack const& attack, Creature& attacker, Creature& attacked)
{
	auto dmg = std::min(process_damage(game_data, attack, attacker, attacked), attacked.hp);
	UI::display(attacker.name + " utilise " + attack.name + " contre " + attacked.name + " !");
	UI::display("Cela lui cause " + std::to_string(dmg) + " points de d�g�ts !");

	attacked.hp -= dmg;
}

/// @copydoc
int process_damage(GameData const& game_data, Attack const& attack, Creature const& attacker, Creature const& attacked)
{
	auto main_coeff = (attacker.level * 0.4 + 2) * attacker.attack * attack.power / (attacked.defense * 50);
	auto STAB{ 1. };
	if (attack.type == attacker.type)
		STAB = 1.5;
	auto efficiency{ get_attack_efficiency(game_data, attack.type, attacked.type) };

	std::uniform_real_distribution<double> dis{ 0.85, 1. };
	auto random_factor{ dis(random_engine) };
	return static_cast<int>(main_coeff * STAB * efficiency * random_factor);
}

/// @copydoc
KOHandlingResult handle_ko(GameData const& game_data, Trainer & trainer, bool wild_opponent)
{
	if (trainer.current_creature->hp > 0)
		return KOHandlingResult::ContinueBattle;
	UI::display("> " + trainer.current_creature->name + " est KO !");
	auto is_not_ko = [](Creature const& c) { return !is_ko(c); };
	if (std::any_of(std::begin(trainer.creatures), std::end(trainer.creatures), is_not_ko))
	{
		auto controller{ trainer.controller };
		ChoicesStack choices{};
		if (controller == Controller::Player)
			choices = ko_menu(trainer);
		else if (controller == Controller::AI)
			choices = AI::choose_ko_handling_action(game_data, trainer);
		if (choices[0] == 0)
		{
			auto creature_choice = choices[1];
			trainer.current_creature = std::begin(trainer.creatures) + creature_choice;
			if (is_ko(*trainer.current_creature))
			{
				assert(trainer.controller == Controller::Player);
				UI::display("Vous ne pouvez pas vous �chapper d'un duel de dresseurs !");
			}
			return KOHandlingResult::ContinueBattle;
		}
		else if (choices[0] == 1)
		{
			if (wild_opponent)
				return KOHandlingResult::Run;

			assert(trainer.controller == Controller::Player);
			UI::display("Vous ne pouvez pas vous �chapper d'un duel de dresseurs !");

			return handle_ko(game_data, trainer, wild_opponent);
		}
		else
			assert(false && "You shouldn't have reached this point.");
	}

	return KOHandlingResult::AllKO;
}
