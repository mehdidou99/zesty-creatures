#include <algorithm>

namespace UI
{
	/// @copydoc
	template<typename ChoicesContainerT>
	int choose(ChoicesContainerT const& choices, bool add_return_option, std::string const& prompt)
	{
		std::vector<std::string> choice_texts{};
		std::transform(std::begin(choices), std::end(choices), std::back_inserter(choice_texts), [](auto const& choice) { return to_choice_text(choice); });
		return choose(choice_texts, add_return_option, prompt);
	}
}
