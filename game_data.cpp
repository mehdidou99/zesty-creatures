#include "game_data.hpp"

#include <algorithm>
#include <cassert>

/// @copydoc
double get_attack_efficiency(GameData const & game_data, Type const& attack_type, Type const& attacked_type)
{
	auto table_entry = std::find_if(std::begin(game_data.efficiency_table), std::end(game_data.efficiency_table),
		[&attack_type, &attacked_type](EfficiencyTableElem const& elem) 
	{
		return std::get<0>(elem) == attack_type && std::get<1>(elem) == attacked_type;
	});
	assert(table_entry != std::end(game_data.efficiency_table) && "Invalid types !");
	return std::get<2>(*table_entry);
}

/// @copydoc
Attack make_attack(GameData const& gd, std::string const& name)
{
	auto attack = gd.attacks.find(name);
	assert(attack != std::end(gd.attacks) && "Invalid attack name.");
	return attack->second;
}

bool is_valid(Type const & type, GameData const & data)
{
	return std::find(std::begin(data.valid_types), std::end(data.valid_types), type) != std::end(data.valid_types);
}
