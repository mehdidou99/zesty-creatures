#ifndef UI_HPP_INCLUDED
#define UI_HPP_INCLUDED

#include "entities.hpp"
#include "menus.hpp"

#include <string>
#include <vector>

namespace UI
{
	/**
	* @brief Returns a choice made by the user.
	* @details This choice is made from a list. This generic version calls the overload for `std::vector<std::string>`.
	*
	* @tparam ChoicesContainerT The type of collection that represent the list of choices.
	*
	* @param[in] choices The list of choices.
	* @param[in] add_return_option Flag that enables the possibility to cancel the choice.
	* @param[in] prompt Optional prompt.
	*/
	template<typename ChoicesContainerT>
	int choose(ChoicesContainerT const& choices, bool add_return_option, std::string const& prompt = "");

	/**
	* @brief Returns a choice made by the user.
	* @details This choice is made from a list. This overload handles `std::vector<std::string>`.
	*
	* @param[in] choices The list of choices.
	* @param[in] add_return_option Flag that enables the possibility to cancel the choice.
	* @param[in] prompt Optional prompt.
	*/
	int choose(std::vector<std::string> const& choices, bool add_return_option, std::string const& prompt = "");

	/**
	* @brief Displays a message to the UI.
	*
	* @param[in] message The message to display.
	*/
	void display(std::string const& message);

	/**
	* @brief Conversion function from `BattleChoice` to `std::string`.
	*/
	std::string to_choice_text(BattleChoice battle_choice);
	/**
	* @brief Conversion function from `Attack` to `std::string`.
	*/
	std::string to_choice_text(Attack const& attack);
	/**
	* @brief Conversion function from `Creature` to `std::string`.
	*/
	std::string to_choice_text(Creature const& creature);
	/**
	* @brief Conversion function from `BagItem` to `std::string`.
	*/
	std::string to_choice_text(BagItem const& item);
	/**
	* @brief Conversion function from `Type` to `std::string`.
	*/
	std::string to_string(Type const& type);
}

#include "UI.tpp"

#endif // !UI_HPP_INCLUDED
