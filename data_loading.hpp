#ifndef DATA_LOADING_HPP_INCLUDED
#define DATA_LOADING_HPP_INCLUDED

#include "game_data.hpp"
#include "entities.hpp"

#include <iostream>
#include <string>

using ParsingError = std::runtime_error;

/**
* @struct ParsingData
* @brief Holds the context that is needed to parse the data files.
*/
struct ParsingContext
{
	/// @brief The stream from which the data is parsed.
	std::istream& data_stream;
	/// @brief The current word in the parser.
	std::string current_word;
};

/**
* @brief Creates a new ParsingContext with the given input stream.
*
* @param data_stream The input stream to use.
*
* @returns The created ParsingContext, initialized with the first word from the stream.
*/
ParsingContext create_parsing_context(std::istream& data_stream);

/**
* @brief Eats the current word and returns it.
* 
* @param pc The current parsing context.
*
* @returns The eaten word.
*/
std::string next_word(ParsingContext& pc);

/**
* @brief Eats the current line (including the current word) and returns it.
*
* @param pc The current parsing context.
*
* @returns The eaten line.
*/
std::string eat_line(ParsingContext& pc);

/**
* @brief Parse a line of data.
* @details A line of data is supposed to be formatted this way : `name = value`. The spaces are important. `value` can contain spaces, but `name` can't.
*
* @param pc The current parsing context.
*
* @returns The pair `{ name, value }`.
*
* @exception ParsingError If the line isn't formatted the right way.
*
* @todo Improve the parser so that it can read a data line without the spaces surrounding the `=`.
*/
std::pair<std::string, std::string> parse_data_line(ParsingContext& pc);

/**
* @brief Parses a line of data and returns the value if the name is the one requested.
* @details A line of data is supposed to be formatted this way : `name = value`. The spaces are important. `value` can contain spaces, but `name` can't.
*
* @param pc The current parsing context.
*
* @returns The pair `{ name, value }`.
*
* @exception ParsingError If the line isn't formatted the right way or if the entry name isn't the good one.
*/
std::string get_entry(ParsingContext& pc, std::string const& entry);

/**
* @brief Parses a line of data and returns the value if the name is the one requested.
* @details A line of data is supposed to be formatted this way : `name = value`. The spaces are important. `value` can contain spaces, but `name` can't.
*
* @tparam The type that will be returned.
* @tparam ConversionFunctionT The type of the conversion function to use.
*
* @param pc The current parsing context.
* @param converter The function conversion to use.
*
* @returns The pair `{ name, value }`.
*
* @exception ParsingError If the line isn't formatted the right way or if the entry name isn't the good one.
*/
template<typename T, typename ConversionFunctionT>
T get_entry(ParsingContext& pc, std::string const& entry, ConversionFunctionT converter, std::string const& fail_prompt = "Invalid entry type.");

/**
* @brief Checks whether the current word is the wanted tag and eats it.
*
* @param pc The current parsing context.
* @param tag The tag to check.
*
* @exception ParsingError If the tag isn't correct.
*/
void check_tag(ParsingContext& pc, std::string const& tag);

/**
* @brief Checks whether the current word is the 'END' tag and eats it.
*
* @param pc The current parsing context.
*
* @exception ParsingError If the tag isn't correct.
*/
void check_end(ParsingContext & pc);

/**
* @brief Parses and loads the valid types of the game.
*
* @param pc The current parsing context.
*
* @returns The loaded instance.
*
* @exception ParsingError If the data format is incorrect.
*/
TypeList load_valid_types(ParsingContext& pc);

/**
* @brief Parses and loads an EfficiencyTableElem instance.
*
* @param pc The current parsing context.
* @param data A game data instance that holds the list of the valid types.
*
* @returns The loaded instance.
*
* @exception ParsingError If the data format is incorrect.
*/
EfficiencyTableElem load_efficiency_entry(ParsingContext& pc, GameData const& data);

/**
* @brief Parses and loads a whole EfficiencyTable instance.
*
* @param pc The current parsing context.
*
* @returns The loaded instance.
*
* @exception ParsingError If the data format is incorrect.
*/
EfficiencyTable load_efficiency_table(ParsingContext& pc, GameData const& data);

/**
* @brief Parses and loads an Attack.
*
* @param pc The current parsing context.
*
* @returns The loaded instance.
*
* @exception ParsingError If the data format is incorrect.
*/
Attack load_attack(ParsingContext& pc);

/**
* @brief Parses and loads a whole AttackTable instance.
*
* @param pc The current parsing context.
*
* @returns The loaded instance.
*
* @exception ParsingError If the data format is incorrect.
*/
AttackTable load_attack_table(ParsingContext & pc);

/**
* @brief Parses and loads an object into an ObjectTable.
*
* @param pc The current parsing context.
* @param table The table to load the object into.
*
* @note The signature differs compared to load_attack for example, and the function relies on side effects to load the object into the table. This choice has been made to avoid load_object_table to have to manage the different types of objects, thus re^specting levels of abstraction better.
*
* @exception ParsingError If the data format is incorrect.
*/
void load_object_into_table(ParsingContext& pc, ObjectTable& table);

/**
* @brief Parses and loads a whole ObjectsTable instance.
*
* @param pc The current parsing context.
*
* @returns The loaded instance.
*
* @exception ParsingError If the data format is incorrect.
*/
ObjectTable load_object_table(ParsingContext& pc);

StageTable load_stage_table(ParsingContext & pc, GameData const& data);

/**
* @brief Parses and loads a whole GameData instance.
*
* @param pc The current parsing context.
*
* @returns The loaded instance.
*
* @exception ParsingError If the data format is incorrect.
*/
GameData load_game_data(ParsingContext& pc);

/**
* @brief Parses and loads a Creature.
*
* @param pc The current parsing context.
*
* @returns The loaded instance.
*
* @exception ParsingError If the data format is incorrect.
*/
Creature load_creature(ParsingContext& pc, GameData const& data);

/**
* @brief Parses and loads a Bag.
*
* @param pc The current parsing context.
*
* @returns The loaded instance.
*
* @exception ParsingError If the data format is incorrect.
*/
Bag load_bag(ParsingContext& pc);

/**
* @brief Parses and loads a BagItem.
* @details A bag item is represented by an entry formatted as follows : `object = name x num_possessed`.
*
* @param pc The current parsing context.
*
* @returns The loaded instance.
*
* @exception ParsingError If the data format is incorrect.
*/
BagItem load_bag_item(ParsingContext& pc);

/**
* @brief Parses and loads a Trainer.
*
* @param pc The current parsing context.
* @param data A GameData instance.
*
* @returns The loaded instance.
*
* @exception ParsingError If the data format is incorrect.
*/
Trainer load_trainer(ParsingContext& pc, GameData const& data);

/**
* @brief Parses and loads a Stage.
*
* @param pc The current parsing context.
* @param data A GameData instance.
*
* @returns The loaded instance.
*
* @exception ParsingError If the data format is incorrect.
*/
Stage load_stage(ParsingContext& pc, GameData const& gd);

#include "data_loading.tpp"

#endif // !DATA_LOADING_HPP_INCLUDED

