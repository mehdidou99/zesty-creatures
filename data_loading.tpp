template<typename T, typename ConversionFunctionT>
T get_entry(ParsingContext& pc, std::string const& entry, ConversionFunctionT converter, std::string const& fail_prompt)
{
	try
	{
		return converter(get_entry(pc, entry));
	}
	catch (std::invalid_argument const&)
	{
		throw ParsingError(fail_prompt);
	}
}
