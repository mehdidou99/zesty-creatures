#include "battle.hpp"
#include "data_loading.hpp"

#include <fstream>
#include <cassert>

Trainer load_trainer_from_file(std::string filename, GameData const& data)
{
	std::ifstream trainer_stream{ filename };
	std::cout << "Reading file '" << filename << "'." << std::endl;
	auto pc = create_parsing_context(trainer_stream);
	return load_trainer(pc, data);
}

int main()
{
	try
	{
		std::ifstream data_stream{ "test_data/game.data" };
		auto data_pc = create_parsing_context(data_stream);
		auto data = load_game_data(data_pc);

		auto mehdi = load_trainer_from_file("test_data/mehdi.trainer", data);
		auto arthur = load_trainer_from_file("test_data/arthur.trainer", data);

	auto br = battle(data, mehdi, arthur);

	if (br == BattleResult::FirstWon)
		std::cout << "Mehdi c'est le boss de la street-zer." << std::endl;
	else if (br == BattleResult::SecondWon)
		std::cout << "Arthur a eu du bol." << std::endl;
	else
		assert(false && "You shouldn't have reached this point");
	}
	catch (ParsingError const& e)
	{
		std::cout << "Erreur de lecture de fichier de donn�es : " << e.what() << std::endl;
	}
	return 0;
}
