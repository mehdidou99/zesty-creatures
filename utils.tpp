#include <limits>

/// @copydoc
template<typename T>
bool secure_input(std::istream & is, T& variable)
{
	if (!(is >> variable))
	{
		is.clear();
		is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		return false;
	}
	return true;
}

/// @copydoc
template<typename T, typename PredicateT>
bool secure_input(std::istream & is, T & variable, PredicateT predicate)
{
	return secure_input(is, variable) && predicate(variable);
}
