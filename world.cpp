#include "world.hpp"

#include "stage.hpp"

std::size_t coord_to_index(std::size_t x, std::size_t y, std::size_t height)
{
	return y * height + x;
}

std::pair<std::size_t, std::size_t> index_to_coord(std::size_t i, std::size_t height)
{
	return { i % height, i / height };
}

StageID & get_stageID(World & world, std::size_t i)
{
	return world.world[i];
}

StageID & get_stageID(World & world, std::size_t x, std::size_t y)
{
	return get_stageID(world, coord_to_index(x, y, world.height));
}
