#include "menus.hpp"

#include "UI.hpp"

#include <cassert>
#include <algorithm>

/// @copydoc
ChoicesStack battle_menu(Trainer const& trainer)
{
	ChoicesStack choices{};
	MenuIndex action_choice{};
	while (std::empty(choices))
	{
		action_choice = UI::choose(std::vector<BattleChoice>{ BattleChoice::Attack, BattleChoice::Creatures, BattleChoice::Bag, BattleChoice::Run }, false, trainer.name + ", c'est votre tour !");
		if (action_choice == 0)
			choices = attack_menu(trainer.current_creature->attacks);
		else if (action_choice == 1)
			choices = creatures_menu(trainer);
		else if (action_choice == 2)
			choices = bag_menu(trainer.bag);
		else if (action_choice == 3)
			return { action_choice };
		else
			assert(false && "You shouldn't have reached this point.");
	}
	choices.insert(std::begin(choices), action_choice);
	return choices;
}

/// @copydoc
ChoicesStack attack_menu(Creature::AttackList const& attacks)
{
	auto attack_choice = UI::choose(attacks, true, "Attaquer !");

	if (attack_choice == -1)
		return {};

	return { attack_choice };
}

/// @copydoc
ChoicesStack creatures_menu(Trainer const& trainer)
{
	auto creature_choice{ UI::choose(trainer.creatures, true, "Choisissez un pok�mon.") };
	if (creature_choice == -1)
		return {};
	return { creature_choice };
}

/// @copydoc
ChoicesStack bag_menu(Bag const & bag)
{
	auto object_choice{ UI::choose(bag, true, "Choisissez un objet.") };
	if (object_choice == -1)
		return {};

	return { object_choice };
}

/// @copydoc
ChoicesStack ko_menu(Trainer const & trainer)
{
	auto choice{ UI::choose(std::vector<BattleChoice>{BattleChoice::Creatures, BattleChoice::Run}, false, trainer.current_creature->name + " est KO !") };
	ChoicesStack choices{};
	if (choice == 0)
		choices = creatures_menu(trainer);
	
	choices.insert(std::begin(choices), choice);
	return choices;
}
