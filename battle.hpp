#ifndef BATTLE_HPP_INCLUDED
#define BATTLE_HPP_INCLUDED

#include "entities.hpp"

struct GameData;

/**
* @enum BattleResult
* @brief Represent the possible outcomes of a battle.
*/
enum class BattleResult
{
	FirstEscaped, SecondEscaped, FirstWon, SecondWon, FirstCaptured, SecondCaptured
};

/**
* @brief Handles the battle between two trainers and returns the outcome.
*
* @param game_data[in] The currently loaded game data.
* @param t1 The first fighter.
* @param t2 The second fighter.
*
* @returns The outcome of the battle.
*/
BattleResult battle(GameData const& game_data, Trainer& t1, Trainer& t2);

/**
* @brief Finds the first creature able to fight and assigns trainer.current_creature to it.
*
* @param trainer The trainer to choose a fighter for.
*/
void set_first_fighter(Trainer& trainer);

/**
* @brief Handles a single battle round.
*
* @param game_data[in] The currently loaded game data.
* @param t1 The first fighter.
* @param t2 The second fighter.
*
* @returns `true` except when `t1` runs away.
*/
bool battle_round(GameData const& game_data, Trainer& t1, Trainer& t2);

/**
* @brief Applies an attack.
*
* @param game_data[in] The currently loaded game data.
* @param[in] attack The attack to apply.
* @param attacker The attacking creature.
* @param attacked The attacked creature.
*/
void apply_attack(GameData const& game_data, Attack const& attack, Creature& attacker, Creature& attacked);

/**
* @brief Processes the damage caused by an, attack.
*
* @param game_data[in] The currently loaded game data.
* @param[in] attack The attack to evaluate.
* @param attacker The attacking creature.
* @param attacked The attacked creature.
*
* @returns The amount of damage to apply.
*/
int process_damage(GameData const& game_data, Attack const& attack, Creature const& attacker, Creature const& attacked);

/**
* @enum KOHandlingResult
* @brief Represents the possible decisions resulting from the handling of a KO.
*/
enum class KOHandlingResult
{
	ContinueBattle, AllKO, Run
};

/**
* @brief Handles a KO.
* @details Decides what to do when a KO occurs using UI or AI depending on the trainer's controller.
*
* @param game_data[in] The currently loaded game data.
* @param trainer The affected trainer.
* @param[in] wild_opponent Flag that enables the Run option.
*
* @returns The decision of the player/AI.
*/
KOHandlingResult handle_ko(GameData const& game_data, Trainer& trainer, bool wild_opponent);

#endif // !BATTLE_HPP_INCLUDED

