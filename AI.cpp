#include "AI.hpp"

#include "random_engine.hpp"

#include <algorithm>
#include <random>
#include <cassert>

namespace AI
{
	/// @copydoc choose_round_action
	ChoicesStack choose_round_action(GameData const & game_data, Trainer const & trainer, Trainer const & opponent)
	{
		std::uniform_int_distribution<int> dis{ 0, static_cast<int>(std::size(trainer.creatures)) };
		return { 0, dis(random_engine) };
	}
	ChoicesStack choose_ko_handling_action(GameData const & game_data, Trainer const & trainer)
	{
		auto is_able = [](Creature const& c)
		{
			return !is_ko(c);
		};
		auto first_able = std::find_if(std::begin(trainer.creatures), std::end(trainer.creatures), is_able);
		assert(first_able != std::end(trainer.creatures) && "No possibility to handle a KO if every creature is KO.");

		return { std::distance(std::begin(trainer.creatures), first_able) };
	}
}
