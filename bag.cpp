#include "bag.hpp"

#include "entities.hpp"
#include "menus.hpp"
#include "game_data.hpp"
#include "random_engine.hpp"

#include <cassert>
#include <algorithm>
#include <random>

/// @copydoc
bool apply_effect(ObjectTable const& ot, BagItem const & object, Trainer & trainer, Trainer& opponent, bool allow_capture)
{
	if (apply_potion(ot, object, trainer))
		return true;
	else if (apply_revive(ot, object, trainer))
		return true;
	else if (allow_capture && apply_capture_device(ot, object, opponent))
		return true;

	return false;
}

/// @copydoc
bool apply_potion(ObjectTable const& ot, BagItem const& object, Trainer & trainer)
{
	auto potion = ot.potions.find(object.name);
	if (potion == std::end(ot.potions))
		return false;

	auto choices = creatures_menu(trainer);
	if (std::empty(choices))
		return false;

	auto choice = choices[0];
	auto hp_amount = potion->second;
	return apply_potion(hp_amount, trainer.creatures[choice]);
}

/// @copydoc
bool apply_potion(int hp_amount, Creature & creature)
{
	if (is_ko(creature) || creature.hp == creature.max_hp)
		return false;

	creature.hp = std::max(creature.hp + hp_amount, creature.max_hp);
	return true;
}

/// @copydoc
bool apply_revive(ObjectTable const & ot, BagItem const & object, Trainer & trainer)
{
	auto revive = ot.revives.find(object.name);
	if (revive == std::end(ot.revives))
		return false;

	auto choices = creatures_menu(trainer);
	if (std::empty(choices))
		return false;

	auto choice = choices[0];
	auto hp_amount = revive->second;
	return apply_revive(hp_amount, trainer.creatures[choice]);
}

/// @copydoc
bool apply_revive(int hp_amount, Creature & creature)
{
	if (!is_ko(creature))
		return false;

	creature.hp = std::max(hp_amount, creature.max_hp);
	return true;
}

/// @copydoc
bool apply_capture_device(ObjectTable const& ot, BagItem const & object, Trainer & opponent)
{
	if (!opponent.wild)
		return false;

	auto cd = ot.capture_devices.find(object.name);
	if (cd == std::end(ot.capture_devices))
		return false;
	
	auto capture_rate = cd->second;
	Creature const& creature = *opponent.current_creature;
	auto capture_prob = std::max((1. - static_cast<double>(creature.hp) / static_cast<double>(creature.max_hp)) * capture_rate, 1.);
	auto dis = std::bernoulli_distribution(capture_prob);
	if (dis(random_engine))
		opponent.captured = true;
	return true;
}
