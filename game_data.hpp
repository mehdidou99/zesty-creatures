#ifndef GAME_DATA_HPP_INCLUDED
#define GAME_DATA_HPP_INCLUDED

#include "entities.hpp"
#include "stage.hpp"

#include <unordered_map>
#include <tuple>

/**
* @struct ObjectsTable
* @brief Table holding the properties of the objects of the game.
*/
struct ObjectTable
{
	/// typedef HpAmount
	using HpAmount = int;
	/// @brief Holds the different potions of the game.
	std::unordered_map<std::string, HpAmount> potions;
	/// @brief Holds the different revive objects of the game.
	std::unordered_map<std::string, HpAmount> revives;

	/// typedef CaptureRate
	using CaptureRate = double;
	/// @brief Holds the different capture devices of the game
	std::unordered_map<std::string, CaptureRate> capture_devices;
};

/// typedef TypeList
using TypeList = std::vector<Type>;

/// typedef EfficiencyTableElem
using EfficiencyTableElem = std::tuple<Type, Type, double>;
/// typedef EfficiencyTable
using EfficiencyTable = std::vector<EfficiencyTableElem>;

/// typedef AttackTable
using AttackTable = std::unordered_map<std::string, Attack>;

///typedef StageTable
using StageTable = std::unordered_map<StageID, Stage>;

/**
* @struct GameData
* @brief Holds all the general data of the game.
*/
struct GameData
{
	/// @brief Lists all the valid types of the game.
	TypeList valid_types;
	/// @brief Holds a table associating Type pairs to the efficiency of an attack of the first type on the second.
	EfficiencyTable efficiency_table;
	/// @brief Holds the different attacks of the game.
	AttackTable attacks;
	/// @brief Holds the different objects of the game.
	ObjectTable objects;
	/// @brief Holds the different stages of the game.
	StageTable stages;
};

/**
* @brief Reads an efficiency table and returns the result.
*
* @param game_data[in] The currently loaded game data.
* @param attack_type The type of the attack.
* @param attacked_type The type of the attacked Creature.
*
* @returns The read efficiency.
*
* @pre The types must be valid types.
*/
double get_attack_efficiency(GameData const& game_data, Type const& attack_type, Type const& attacked_type);

/**
* @brief Create an Attack instance from an attack name.
*
* @param gd[in] The currently loaded game data.
* @param name The name of the attack.
*
* @returns An Attack instance.
*
* @pre The attack name must be valid.
*/
Attack make_attack(GameData const& gd, std::string const& name);

bool is_valid(Type const& type, GameData const& data);

#endif // !GAME_DATA_HPP_INCLUDED

