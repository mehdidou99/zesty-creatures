#ifndef ENTITIES_HPP_INCLUDED
#define ENTITIES_HPP_INCLUDED

#include <array>
#include <vector>
#include <string>
#include <optional>

/**
* @typedef Type
* @brief Represents a Creature type.
*/
using Type = std::string;

/**
* @struct Attack
* @brief Holds the data of an attack.
*/
struct Attack
{
	/// @brief The name of the attack.
	std::string name;
	/// @brief The type of the attack.
	Type type;
	/// @brief The power of the attack.
	int power;
};

/**
* @struct Creature
* @brief Holds the data of a creature.
*/
struct Creature
{
	/// @typedef AttackList
	using AttackList = std::vector<Attack>;

	/// @brief The name of the creature.
	std::string name;

	/// @brief The HP of the creature.
	int hp;
	/// @brief The maximum HP that the creature can have.
	int max_hp;
	/// @brief The level of the creature.
	int level;
	/// @brief The offensive power of the creature.
	int attack;
	/// @brief The defensive power of the creature.
	int defense;
	/// @brief The type of the creature.
	Type type;
	/// @brief The attacks of the creature.
	AttackList attacks;
};

/**
* @brief Helper function that returns if the given Creature is KO.
*
* @param[in] creature The Creature to test.
*
* @returns `true` if the Creature is KO, `false` if not.
*/
bool is_ko(Creature const& creature);

/**
* @struct BagItem
* @brief Stores the data of an item of the bag.
*/
struct BagItem
{
	/// @brief The name of the object.
	std::string name;

	/// @brief The number of objects of this type that are in the bag.
	int number_possessed;
};

/**
* @typedef Bag
* @brief Represent a bag (as a collection of BagItem).
*/
using Bag = std::vector<BagItem>;

/**
* @enum Controller
* @brief Represents the different possible controllers of a trainer.
*/
enum class Controller
{
	Player
	, AI
};

/**
* @struct Creature
* @brief Holds the data of a trainer.
*/
struct Trainer
{
	/// @typedef CreatureList
	using CreatureList = std::vector<Creature>;

	/// @brief The name of the Trainer.
	std::string name;
	/// @brief Flag : `true` if the trainer is actually a wild creature, false if it isn't.
	bool wild;
	/// @brief Flag : `true` if the creature is captured (only possible when `wild` is also `true`).
	bool captured{ false };
	/// @brief The controller of the Trainer.
	Controller controller;
	/// @brief The creatures of the Trainer.
	CreatureList creatures;
	/// @brief Iterator designing the Trainer's creature that is currently battling.
	CreatureList::iterator current_creature;
	/// @brief The bag of the Trainer.
	Bag bag;
};

#endif // !ENTITIES_HPP_INCLUDED
