#include "data_loading.hpp"

#include "utils.hpp"

#include <stdexcept>
#include <sstream>
#include <iostream>

/// @copydoc
ParsingContext create_parsing_context(std::istream & data_stream)
{
	ParsingContext pc{ data_stream, "" };
	next_word(pc);
	return pc;
}

/// @copydoc
std::string next_word(ParsingContext & pc)
{
	std::string current = pc.current_word;
	if (pc.data_stream >> pc.current_word)
		return current;
	else if (pc.data_stream.eof())
	{
		pc.current_word = "EOF";
		return current;
	}

	throw ParsingError("Impossible to get next word : invalid data stream.");
}

/// @copydoc
std::string eat_line(ParsingContext & pc)
{
	std::string line{};
	std::getline(pc.data_stream, line);
	line = pc.current_word + line;
	next_word(pc);
	return line;
}

/// @copydoc
std::pair<std::string, std::string> parse_data_line(ParsingContext & pc)
{
	auto name = next_word(pc);
	if (next_word(pc) != "=")
		throw ParsingError("Invalid data line. The format must be 'name = value'");
	auto value = eat_line(pc);
	return { name, value };
}

/// @copydoc
std::string get_entry(ParsingContext & pc, std::string const & wanted_label)
{
	auto[label, value] = parse_data_line(pc);
	if (label != wanted_label)
		throw ParsingError("Expected a '" + wanted_label + "' entry, got '" + label + "'.");
	return value;
}

/// @copydoc
void check_tag(ParsingContext & pc, std::string const & tag)
{
	auto word = next_word(pc);
	std::cout << "Loading a '" << tag << "' object." << std::endl;
	if (word != tag)
		throw ParsingError("Expected a '" + tag + "' tag, got " + word + '.');
}

/// @copydoc
void check_end(ParsingContext & pc)
{
	check_tag(pc, "END");
}

/// @copydoc
TypeList load_valid_types(ParsingContext & pc)
{
	check_tag(pc, "VALID_TYPES");
	auto line = eat_line(pc);
	check_end(pc);
	return split(line, ',');
}

/// @copydoc
EfficiencyTableElem load_efficiency_entry(ParsingContext & pc, GameData const & data)
{
	auto[type_pair, eff_str] = parse_data_line(pc);
	auto efficiency = std::stod(eff_str);
	auto types = split(type_pair, ',');
	if (std::size(types) != 2 || !is_valid(types[0], data) || !is_valid(types[1], data))
		throw ParsingError("Invalid efficiency entry : the types must be valid.");
	return { types[0], types[1], efficiency };
}

/// @copydoc
EfficiencyTable load_efficiency_table(ParsingContext & pc, GameData const& data)
{
	check_tag(pc, "EFFICIENCY_TABLE");
	EfficiencyTable table{};
	while (pc.current_word != "END")
		table.push_back(load_efficiency_entry(pc, data));
	check_end(pc);
	return table;
}

/// @copydoc
Attack load_attack(ParsingContext & pc)
{
	check_tag(pc, "ATTACK");
	Attack attack{};
	attack.name = get_entry(pc, "name");
	attack.type = get_entry(pc, "type");
	attack.power = std::stoi(get_entry(pc, "power"));
	check_end(pc);
	return attack;
}

/// @copydoc
AttackTable load_attack_table(ParsingContext & pc)
{
	check_tag(pc, "ATTACK_TABLE");
	AttackTable attacks{};
	while (pc.current_word == "ATTACK")
	{
		auto attack = load_attack(pc);
		attacks[attack.name] = attack;
	}
	check_end(pc);

	return attacks;
}

void load_object_into_table(ParsingContext & pc, ObjectTable & table)
{
	check_tag(pc, "OBJECT");
	auto type = get_entry(pc, "type");
	auto name = get_entry(pc, "name");
	auto str_to_hp = [](std::string const& s) { return std::stoi(s); };
	if (type == "potion")
		table.potions[name] = get_entry<ObjectTable::HpAmount>(pc, "hp_amount", str_to_hp, "The hp amount of a potion must be an integer.");
	else if (type == "revive")
		table.revives[name] = get_entry<ObjectTable::HpAmount>(pc, "hp_amount", str_to_hp, "The hp amount of a revive must be an integer.");
	else if (type == "capture")
	{
		auto str_to_cr = [](std::string const& s) { return std::stod(s); };
		table.capture_devices[name] = get_entry<ObjectTable::CaptureRate>(pc, "rate", str_to_cr, "The capture rate of a capture device must be a real number.");
	}
	check_end(pc);
}

ObjectTable load_object_table(ParsingContext & pc)
{
	check_tag(pc, "OBJECT_TABLE");
	ObjectTable objects{};
	while (pc.current_word == "OBJECT")
		load_object_into_table(pc, objects);
	check_end(pc);
	return objects;
}

/// @copydoc
StageTable load_stage_table(ParsingContext & pc, GameData const& data)
{
	check_tag(pc, "STAGE_TABLE");
	StageTable stages{};
	while (pc.current_word == "STAGE")
	{
		auto stage = load_stage(pc, data);
		stages[stage.id] = stage;
	}
	check_end(pc);

	return stages;
}

/// @copydoc
GameData load_game_data(ParsingContext & pc)
{
	check_tag(pc, "GAME_DATA");
	
	GameData data {
		load_valid_types(pc)
		, load_efficiency_table(pc, data)
		, load_attack_table(pc)
		, load_object_table(pc)
		, load_stage_table(pc, data)
	};

	check_end(pc);

	return data;
}

/// @copydoc
Creature load_creature(ParsingContext & pc, GameData const& data)
{
	check_tag(pc, "CREATURE");

	Creature creature{};

	creature.name = get_entry(pc, "name");
	creature.hp = std::stoi(get_entry(pc, "hp"));
	creature.max_hp = std::stoi(get_entry(pc, "max_hp"));
	creature.level = std::stoi(get_entry(pc, "level"));
	creature.attack = std::stoi(get_entry(pc, "attack"));
	creature.defense = std::stoi(get_entry(pc, "defense"));
	creature.type = get_entry(pc, "type");

	auto i{ 0 };
	while (i < 4 && pc.current_word != "END")
	{
		++i;
		auto name = get_entry(pc, "att");
		creature.attacks.push_back(make_attack(data, name));
	}

	check_end(pc);

	return creature;
}

/// @copydoc
Bag load_bag(ParsingContext & pc)
{
	check_tag(pc, "BAG");

	Bag bag{};
	while (pc.current_word != "END")
	{
		bag.push_back(load_bag_item(pc));
	}

	check_end(pc);

	return bag;
}

/// @copydoc
BagItem load_bag_item(ParsingContext & pc)
{
	std::istringstream object_stream{ get_entry(pc, "object") };
	std::string name{};
	std::string cur_word{};
	while (cur_word != "x")
	{
		if (!(object_stream >> cur_word))
			throw ParsingError("Error during BagItem parsing.");
		name += cur_word;
	}
	int num_possessed{};
	if(!object_stream >> num_possessed)
		throw ParsingError("Error during BagItem parsing : expected a number.");
	return BagItem{ name, num_possessed };
}

/// @copydoc
Trainer load_trainer(ParsingContext & pc, GameData const& data)
{
	check_tag(pc, "TRAINER");

	Trainer trainer{};

	trainer.name = get_entry(pc, "name");
	auto wild_s = get_entry(pc, "wild");
	if (wild_s == "true")
		trainer.wild = true;
	else if (wild_s == "false")
		trainer.wild = false;
	else
		throw ParsingError("Invalid value for the 'wild' entry : expected 'true' or 'false', got '" + wild_s + "'.");

	auto controller_s = get_entry(pc, "controller");
	if (controller_s == "player")
		trainer.controller = Controller::Player;
	else if (controller_s == "AI")
		trainer.controller = Controller::AI;
	else
		throw ParsingError("Invalid value for the 'controller' entry : expected 'player' or 'AI', got '" + controller_s + "'.");

	auto creatures_num{0};
	while (creatures_num < 6 && pc.current_word == "CREATURE")
	{
		++creatures_num;
		trainer.creatures.push_back(load_creature(pc, data));
	}
	trainer.bag = load_bag(pc);
	check_end(pc);

	return trainer;
}

/// @copydoc
Stage load_stage(ParsingContext & pc, GameData const& gd)
{
	check_tag(pc, "STAGE");

	Stage stage{};
	stage.id = std::stoi(get_entry(pc, "id"));
	while (pc.current_word == "TRAINER")
		stage.trainers.push_back(load_trainer(pc, gd));
	while (pc.current_word == "CREATURE")
		stage.wild_creatures.push_back(load_creature(pc, gd));
	stage.wild_encounter_prob = std::stod(get_entry(pc, "wild_prob"));
	stage.mandatory_wild_encounters = std::stoi(get_entry(pc, "mandatory_encounters"));

	check_end(pc);

	return stage;
}
