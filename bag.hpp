#ifndef BAG_HPP_INCLUDED
#define BAG_HPP_INCLUDED

#include "entities.hpp"
#include "game_data.hpp"

#include <string>

/**
* @brief Apply the effect of a given object to the trainer (or to the opponent if it's a capture device).
* 
* @param[in] ot The objects table of the game.
* @param[in] object The object to apply.
* @param trainer The trainer that uses the object.
* @param[in] allow_capture Flag that enables the use of capture devices.
*
* @returns Whereas the object has been successfully applied or not.
*/
bool apply_effect(ObjectTable const& ot, BagItem const& object, Trainer& trainer, Trainer& opponent, bool allow_capture = false);

/**
* @brief Apply the effect of a potion.
*
* @param[in] ot The objects table of the game.
* @param[in] object The potion to apply.
* @param trainer The trainer that uses the potion.
*
* @returns Whereas the object has been successfully applied or not.
*/
bool apply_potion(ObjectTable const& ot, BagItem const& object, Trainer& trainer);

/**
* @brief Actually applies a potion on a creature.
*
* @param[in] ot The objects table of the game.
* @param[in] hp_amount The amount of HP that the potion can restore.
* @param[out] trainer The target creature.
* @param[in] ot The objects table of the game.
*
* @returns Whereas the object has been successfully applied or not.
*/
bool apply_potion(int hp_amount, Creature& creature);

/**
* @brief Apply the effect of a revival object.
*
* @param[in] ot The objects table of the game.
* @param[in] object The revival object to apply.
* @param trainer The trainer that uses the object.
* @param[in] ot The objects table of the game.
*
* @returns Whereas the object has been successfully applied or not.
*/
bool apply_revive(ObjectTable const& ot, BagItem const& object, Trainer& trainer);

/**
* @brief Actually applies a revival object on a creature.
*
* @param[in] ot The objects table of the game.
* @param[in] hp_amount The amount of HP that the revival object can restore.
* @param[out] trainer The target creature.
*
* @returns Whereas the object has been successfully applied or not.
*/
bool apply_revive(int hp_amount, Creature& creature);

/**
* @brief Apply the effect of a capture device.
*
* @param[in] ot The objects table of the game.
* @param[in] object The capture device to apply.
* @param trainer The trainer that uses the device.
*
* @returns Whereas the object has been successfully applied or not.
*/
bool apply_capture_device(ObjectTable const& ot, BagItem const& object, Trainer& opponent);

#endif // !BAG_HPP_INCLUDED
