#include "UI.hpp"

#include "utils.hpp"
#include "menus.hpp"

#include <iostream>
#include <unordered_map>
#include <sstream>
#include <cassert>

namespace UI
{
	/// @copydoc
	int choose(std::vector<std::string> const & choices, bool add_return_option, std::string const & prompt)
	{
		if (prompt != "")
			std::cout << "> " << prompt << std::endl;
		auto s{ static_cast<int>(std::size(choices)) };
		for (auto i{ 0 }; i < s; ++i)
			std::cout << "> " << i + 1 << ") " << choices[i] << std::endl;
		if (add_return_option)
			std::cout << "> " << s + 1 << ") Retour" << std::endl;

		auto choices_number{ add_return_option ? s+1 : s };
		int choice{};
		std::cout << ">>> ";
		while (!secure_input(std::cin, choice, [choices_number](int c) {return 1 <= c && c <= choices_number; }))
		{
			std::cout << "> Choix invalide ! Tapez le num�ro de votre choix., entre 1 et " << choices_number << std::endl;
			std::cout << ">>> ";
		}
		choice -= 1;
		if (add_return_option && choice == std::size(choices))
			choice = -1;

		return choice;
	}

	/// @copydoc
	void display(std::string const & message)
	{
		std::cout << "> " << message << std::endl;
	}

	/// @copydoc
	std::string to_choice_text(BattleChoice battle_choice)
	{
		if (battle_choice == BattleChoice::Attack)
			return "Attaquer";
		if (battle_choice == BattleChoice::Creatures)
			return "Cr�atures";
		if (battle_choice == BattleChoice::Bag)
			return "Sac";
		if (battle_choice == BattleChoice::Run)
			return "Fuite";

		assert(false && "You shouldn't have reached this point.");

		return "";
	}

	/// @copydoc
	std::string to_choice_text(Attack const & attack)
	{
		using namespace std::literals;
		return attack.name + " | "s + to_string(attack.type);
	}

	/// @copydoc
	std::string to_choice_text(Creature const & creature)
	{
		std::ostringstream oss{};
		std::string sep = " | ";
		oss << creature.name <<  sep << "Level " << creature.level << sep << "HP : " << creature.hp << '/' << creature.max_hp;
		return oss.str();
	}

	/// @copydoc
	std::string to_choice_text(BagItem const & item)
	{
		std::ostringstream oss;
		oss << item.name << " x" << item.number_possessed;
		return oss.str();
	}

	/// @copydoc
	std::string to_string(Type const& type)
	{
		return type;
	}
}
